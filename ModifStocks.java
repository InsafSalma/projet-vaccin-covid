package modules;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.Dimension;
import java.awt.Window;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Permet de cr�er la fenetre d'ajout ou de modification de stocks
 *
 */
public class ModifStocks extends JFrame implements ActionListener
{
	/**
	 * numero de version pour classe serialisable
	 */
	private static final long serialVersionUID = 1L; 
	
	//Un conteneur
	private JPanel containerPanel;
	private JTextField tfNum = new JTextField();
	private JTextField tfFour = new JTextField();
	private JTextField tfVac = new JTextField();
	private JTextField tfDate = new JTextField();
	private JTextField tfNombre = new JTextField();
	private JButton save, close;
	
	JPanel pNum, pFour, pVac, pDate, pNombre, pButtons;
	
	int stateWindow = -1; //This integer is used to determine whether the window is used to add, modify or just see the data of a patient
	
	private StockDAO stockDAO;
	Stock stock;
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	public Stock getStock() { return this.stock; }
	
	class Temp { public GestionFenetre gF; public int state = -1; public String button; public Stock stockData;}
	final Temp tmp = new Temp();
	
	/**
	 * Constructeur
	 * Definit la fenetre et ses composants - affiche la fenetre
	 */
	public ModifStocks(int state, Stock stock,  GestionFenetre gF)
    {
		tmp.gF = gF;
		tmp.state = state;
		stockDAO= new StockDAO();
		containerPanel = new JPanel();
		stateWindow = state;
		this.stock = stock;
		JLabel num = new JLabel();
		JLabel four = new JLabel();
		JLabel vac = new JLabel();
		JLabel date = new JLabel();
		JLabel nombre = new JLabel();

		save = new JButton("Enregistrer");
		close = new JButton("Close");
		save.addActionListener(this);
		close.addActionListener(this);
		
		num.setText("Numero : ");
		four.setText("Fournisseur : ");
		vac.setText("Nom du vaccin : ");
		date.setText("Date d'entree : ");
		nombre.setText("Nombre de stock : ");

		
		tfNum.setText(Integer.toString(stock.getNumero()));
		tfFour.setText(stock.getNomFournisseur());
		tfVac.setText(stock.getNomVaccin());
		tfDate.setText(df.format(stock.getDateEntree()));
		tfNombre.setText(Integer.toString(stock.getNombre()));

		
		//Etat = 1 => Etat "Ajout";
		//Etat = 2 => Etat "Modification"
		//État = 3 => Etat "Obtenir plus d'information"
		tfNum.setEnabled(false);
		if(stateWindow == 3) {
			tfFour.setEnabled(false);
			tfVac.setEnabled(false);
			tfDate.setEnabled(false);
			tfNombre.setEnabled(false);
			save.setEnabled(false);
		}
		
		
		Dimension boxDimPass = tfNum.getMinimumSize(); boxDimPass.setSize(boxDimPass.getWidth()+800, boxDimPass.getHeight());
		tfNum.setMaximumSize(boxDimPass);
		tfFour.setMaximumSize(boxDimPass);
		tfVac.setMaximumSize(boxDimPass);
		tfDate.setMaximumSize(boxDimPass);
		tfNombre.setMaximumSize(boxDimPass);
		
		pNum = new JPanel();
		pFour = new JPanel();
		pVac = new JPanel();
		pDate = new JPanel();
		pNombre = new JPanel();
		pButtons = new JPanel();
		
		pNum.setLayout(new BoxLayout(pNum, BoxLayout.LINE_AXIS));
		pFour.setLayout(new BoxLayout(pFour, BoxLayout.LINE_AXIS));
		pVac.setLayout(new BoxLayout(pVac, BoxLayout.LINE_AXIS));
		pDate.setLayout(new BoxLayout(pDate, BoxLayout.LINE_AXIS));
		pNombre.setLayout(new BoxLayout(pNombre, BoxLayout.LINE_AXIS));
		
		pNum.add(num);
		pNum.add(Box.createRigidArea(new Dimension(10,0)));
		pNum.add(tfNum);
		
		pFour.add(four);
		pFour.add(Box.createRigidArea(new Dimension(10,0)));
		pFour.add(tfFour);
		
		pVac.add(vac);
		pVac.add(Box.createRigidArea(new Dimension(10,0)));
		pVac.add(tfVac);
		
		pDate.add(date);
		pDate.add(Box.createRigidArea(new Dimension(10,0)));
		pDate.add(tfDate);
		
		pNombre.add(nombre);
		pNombre.add(Box.createRigidArea(new Dimension(10,0)));
		pNombre.add(tfNombre);
		
		pButtons.add(Box.createRigidArea(new Dimension(10,0)));
		pButtons.add(save);
		pButtons.add(Box.createRigidArea(new Dimension(10,0)));
		pButtons.add(close);
		pButtons.add(Box.createRigidArea(new Dimension(10,0)));
		
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
		containerPanel.add(Box.createRigidArea(new Dimension(0,50)));
		containerPanel.add(pNum);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pFour);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pVac);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pDate);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pNombre);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pButtons);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pButtons);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setContentPane(containerPanel);
		this.setTitle("Ajouter/modifier/consulter un patient");
		this.setSize(new Dimension(1000,400));
		this.setVisible(true);
		
		WindowListener exitListener = new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
            	if(tmp.button == "save") {
            		if(tmp.state == 1) {
                		tmp.gF.addLineToStock(tmp.stockData);
                		System.out.println("Adding stock ...");
                	} else if (tmp.state == 2) {
                		tmp.gF.modifyLineToStock(tmp.stockData);
                		System.out.println("Modifying stock ...");
                	}
            	} else {
            		System.out.println("Closing without saving ...") ;
            	}
            }
        };
        this.addWindowListener(exitListener);
	}

	/**
	 * Permet de vider les champs textes de la page
	 *
	 */
	 public void clear_fields()
	    {
	    	this.tfNum.setText("");
	    	this.tfFour.setText("");
	    	this.tfVac.setText("");
	    	this.tfDate.setText("");
	    	this.tfNombre.setText("");
	    }
	 
	 
		public void modify_stock()
		{
			try {
				String target = this.tfDate.getText();
				Date date;
				date = df.parse(target);
				stock.setNomFournisseur(this.tfFour.getText());
				stock.setNomVaccin(this.tfVac.getText());
				stock.setDateEntree(date);
				stock.setNombre(Integer.valueOf(this.tfNombre.getText()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	/**
	 * Permet de g�rer les actions r�alis�es par les boutons
	 */
	public void actionPerformed(ActionEvent ae)
	{
		Object source = ae.getSource();
		if(source == save) {
			tmp.button = "save";
			String target = tfDate.getText();
			try {
				Date date;
				date = df.parse(target);
				tmp.stockData = new Stock(tfFour.getText(), tfVac.getText(), date, Integer.parseInt(tfNombre.getText()));
				Window window = SwingUtilities.windowForComponent((JButton)ae.getSource());
		        window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
		        if(stateWindow == 1) {
					//Todo : ajout du stock
					modify_stock();
					stockDAO.ajouter(stock);
					tmp.gF.updateStockTable();
					clear_fields();
				} else if (stateWindow == 2) {
					//Todo : modification du stock
					modify_stock();
					stockDAO.modifier(stock);
					tmp.gF.updateStockTable();
				}
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		} else if(source == close) {
			Window window = SwingUtilities.windowForComponent((JButton)ae.getSource());
	        window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
			tmp.button = "close";
		}
	}


	public static void main(String[] args)
	{
		//new ModifStocks(1, new Stock("Apple", "Vaccin", new Date(), 42));
    }

}
