package modules;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.Dimension;
import java.awt.KeyboardFocusManager;
import java.awt.Window;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

/**
 * Cette classe permet de générer le menu permettant la gestion des stocks et des patients
 *
 */
public class GestionFenetre extends JFrame implements ActionListener
{
	/**
	 * numero de version pour classe serialisable
	 */
	private static final long serialVersionUID = 1L; 
	
	/**
	 * Le conteneur de la page
	 */
	private JPanel containerPanel;		
	
	/**
	 * Une classe temporaire utilisé pour passer des variables de la classes dans les différents listener
	 *
	 */
	class Temp { public String[] columnName = {""} ;public Object[][] data = {}; public String filter = "";}
	/**
	 * Une instance de la classe temporaire 
	 * @see Temp
	 */
	final Temp tableData = new Temp();
	
	/**
	 * Button permettant de switcher entre le tableau de stock et le tableau de patients
	 */
	private JToggleButton stock, patients;
	
	/**
	 * La table contenant les données des stocks et la table contenant les données des patients
	 */
	private JTable tabStock, tabPatients;
	
	/**
	 * Le model des deux tables de stocks et de patients
	 * @see tabStock
	 * @see tabPatients
	 */
	private DefaultTableModel modelStock, modelPatients;
	
	/**
	 * Un scrollPane contenant les tableaux, afin de pouvoir parcourir le tableau lorsqu'il y a un nombre de lignes conséquent
	 */
	private JScrollPane scrollPane;
	
	/**
	 * La barre de recherche présente sur l'interface pour effectuer une recherche sur les deux tables
	 * @see tabStock
	 * @see tabPatients
	 */
	private JTextField searchBar;
	
	/**
	 * Deux filtres utilisés pour effectuer la recherche dans les deux tables
	 * @see tabStock
	 * @see tabPatients
	 * @see searchBar
	 */
	TableRowSorter<TableModel> sorterStock, sorterPatients;
	
	/**
	 * Le bouton permettant d'ajouter une ligne dans une des deux tables
	 */
	JButton ajout;
	
	/**
	 * Le bouton permettant de pouvoir accéder aux informations d'une ligne de données
	 */
	JButton plus;
	
	/**
	 * Le bouton permettant de supprimer une ligne de l'une des deux tables, et de la base de données
	 */
	JButton supprimer;
	
	/**
	 * Un format de date, utilisé pour convertir les dates sous forme de String en objet Date
	 */
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	/**
	 * Permet d'obtenir les données de patients depuis la base de données
	 */
	private PatientDAO patientDAO;
	
	/**
	 * Permet d'obtenir les données des stocks depuis la base de données
	 */
	private StockDAO stockDAO;
	
	/**
	 * Permet d'obtenir les données liées aux injections depuis la base de données
	 */
	private InjectionDAO injectionDAO;
	
	
	/**
	 * Constructeur
	 * D�finit la fen�tre et ses composants - affiche la fen�tre 
	 */
	public GestionFenetre()
    {
		stockDAO= new StockDAO();
		patientDAO = new PatientDAO();
		injectionDAO= new InjectionDAO();
		containerPanel = new JPanel();
		
		//North part of the window : the button
		stock = new JToggleButton("Consulter les stocks");
		patients = new JToggleButton("Consulter les patients");
		stock.setAlignmentX(Component.CENTER_ALIGNMENT);
		patients.setAlignmentX(Component.CENTER_ALIGNMENT);
		stock.addActionListener(this);
		patients.addActionListener(this);
		
		//Button stock and patients to change the table
		JPanel panelButton = new JPanel();
		panelButton.setLayout(new BoxLayout(panelButton, BoxLayout.LINE_AXIS));
		panelButton.add(stock);
		panelButton.add(Box.createRigidArea(new Dimension(100,0)));
		panelButton.add(patients);
		
		//Lower-North part of the window : the search bar
		searchBar = new JTextField();
		//searchBar.setText("rechercher");
		searchBar.setAlignmentX(Component.LEFT_ALIGNMENT);
		Dimension boxDimSearch = searchBar.getMinimumSize(); boxDimSearch.setSize(boxDimSearch.getWidth()+500, boxDimSearch.getHeight());
		searchBar.setMaximumSize(boxDimSearch);
		searchBar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				tableData.filter = searchBar.getText().toString();
				sorterStock.setRowFilter(RowFilter.regexFilter(tableData.filter));	
				sorterPatients.setRowFilter(RowFilter.regexFilter(tableData.filter));
				System.out.println("search for : " + tableData.filter);
				tabPatients.getSelectionModel().clearSelection();
				tabStock.getSelectionModel().clearSelection();
			}
		});
		
		//Center part of the window : the table
		tableData.columnName = new String[]{"Numero", "Fournisseur", "Nom du vaccin", "Date d'entree", "Nombre de stock"};
		//Data for the JTable for stocks
		//Note : haven't found any ways for the time being to refresh or change the value after this part.
		/*tableData.data = new Object[][]{
				{11, "Ananas", "Vaccins", new Date().toString(), "42"}, 
				{12, "Pizza", "Vaccins2", new Date().toString(), "62"},
				{11, "Ananas", "Vaccins", new Date().toString(), "42"}
				};*/
		modelStock = new DefaultTableModel(tableData.data, tableData.columnName);
		tabStock = new JTable(modelStock) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tabStock.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		/*sorterStock = new TableRowSorter<TableModel>(tabStock.getModel());
		sorterStock.setRowFilter(RowFilter.regexFilter(tableData.filter, 0, 1));
		tabStock.setRowSorter(sorterStock);*/
		tableData.columnName = new String[]{"Id", "Nom", "Prenom", "Age", "Phase", "Profession", "Medecin traitant", "Date d'injection"};
		//Data for the JTable for patients
		/*tableData.data = new Object[][]{
			{11, "Jean", "Tleman", "42", "1", "Prof", "Jean Soigne",  "42/01/1983"}, 
			{12, "Jean", "Tleman", "43", "2", "Prof", "Jean Aide", "42/01/1983"},
			{13, "Jean", "Tleman", "44", "", "Prof", "Jean vaccine", "42/01/1983"}
			};*/
		modelPatients = new DefaultTableModel(tableData.data, tableData.columnName);
		tabPatients = new JTable(modelPatients) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		tabPatients.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		/*sorterPatients = new TableRowSorter<TableModel>(tabPatients.getModel());
		sorterPatients.setRowFilter(RowFilter.regexFilter(tableData.filter, 0, 1));
		tabPatients.setRowSorter(sorterPatients);*/
		
		scrollPane = new JScrollPane();
		scrollPane.setPreferredSize(new Dimension(500,200));
		scrollPane.setMinimumSize(new Dimension(500,200));
		
		//Lower part of the window : the buttons
		ajout = new JButton("Ajouter/Modifier");
		plus = new JButton("Voir plus de details");
		supprimer = new JButton("Supprimer");
		ajout.addActionListener(this);
		plus.addActionListener(this);
		supprimer.addActionListener(this);
		JPanel panelButton2 = new JPanel();
		panelButton2.add(ajout);
		panelButton2.add(Box.createRigidArea(new Dimension(100,0)));
		panelButton2.add(plus);
		panelButton2.add(Box.createRigidArea(new Dimension(100,0)));
		panelButton2.add(supprimer);
		
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		containerPanel.add(panelButton);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		containerPanel.add(searchBar);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		containerPanel.add(scrollPane);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		containerPanel.add(panelButton2);
		containerPanel.add(Box.createRigidArea(new Dimension(0,20)));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(containerPanel);
		this.setTitle("Gestion des stocks et des patients");
		this.setSize(new Dimension(1000,500));
		this.setVisible(true);
	}
	
	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae)
	{
		Object source = ae.getSource();
		if(source == stock) { // Stock button : to put the stock table
			scrollPane.setViewportView(tabStock);
			stock.setSelected(true);
			patients.setSelected(false);
			tabPatients.getSelectionModel().clearSelection();
			updateStockTable();
		} else if (source == patients) { // Patient button : to put the patient table
			scrollPane.setViewportView(tabPatients);
			stock.setSelected(false);
			patients.setSelected(true);
			tabStock.getSelectionModel().clearSelection();
			updatePatientTable();
			
		} else if (source == ajout) { // Add button
			if(stock.isSelected()) { // If the current table is the stock table
				if(tabStock.getSelectedRow() != -1) { //If a row is selected
					Vector objTemp = modelStock.getDataVector().elementAt(tabStock.getSelectedRow());
					int nombre;
					if(objTemp.get(4).toString() == "") {
						nombre = 0;
					} else {
						nombre = Integer.parseInt(objTemp.get(4).toString());
					}
					String target = objTemp.get(3).toString();
					try {
						Date date;
						date = df.parse(target);
						Stock tmpS = new Stock(Integer.parseInt(objTemp.get(0).toString()),objTemp.get(1).toString(), objTemp.get(2).toString(), date, nombre);
						System.out.println( objTemp.get(3).toString());
						System.out.println(date.toString());
						new ModifStocks(2, tmpS, this);
						
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
			        
				} else { //If no row is selected
					
					new ModifStocks(1, new Stock("", "", new Date(), 0), this);
				}
				
			} else if (patients.isSelected()) { //If the current table is the patient table
				if(tabPatients.getSelectedRow() != -1) { //If a row is selected
					Vector objTemp = modelPatients.getDataVector().elementAt(tabPatients.getSelectedRow());
					int age;
					if(objTemp.get(3).toString()=="") {
						 age = 0;
					} else {
						age = Integer.parseInt(objTemp.get(3).toString());
					}
					int phase;
					if(objTemp.get(4).toString() == "") {
						phase = 0;
					} else {
						phase = Integer.parseInt(objTemp.get(4).toString());
					}
					Patient tmpP = new Patient(Integer.parseInt(objTemp.get(0).toString()),objTemp.get(1).toString(), objTemp.get(2).toString(), age, phase, objTemp.get(5).toString(), objTemp.get(6).toString());
					new ModifPatient(2, tmpP, this);
				} else { //If no row is selected
					new ModifPatient(1, new Patient("", "", 0, 0, "", ""), this);
				}
			}else {
				JOptionPane.showMessageDialog(containerPanel,"Veuillez chooisir une liste! ");
			}
			
		} else if (source == plus) { // The see more details button
			if(stock.isSelected()) { //If the row selected is in the stock table
				if(tabStock.getSelectedRow() != -1) {
					//TODOCharger le patient directement
					Vector objTemp = modelStock.getDataVector().elementAt(tabStock.getSelectedRow());
					int nombre;
					if(objTemp.get(4).toString() == "") {
						nombre = 0;
					} else {
						nombre = Integer.parseInt(objTemp.get(4).toString());
					}
					String target = objTemp.get(3).toString();
					try {
						Date date;
						date = df.parse(target);
						Stock tmpS = new Stock(Integer.parseInt(objTemp.get(0).toString()),objTemp.get(1).toString(), objTemp.get(2).toString(), date, nombre);
						new ModifStocks(3, tmpS, this);
					} catch (ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			} else if (patients.isSelected()) { //If the row selected is in the patient table
				if(tabPatients.getSelectedRow() != -1) {
					//TODOCharger le patient directement
					Vector objTemp = modelPatients.getDataVector().elementAt(tabPatients.getSelectedRow());
					int age;
					if(objTemp.get(3).toString()=="") {
						 age = 0;
					} else {
						age = Integer.parseInt(objTemp.get(3).toString());
					}
					int phase;
					if(objTemp.get(4).toString() == "") {
						phase = 0;
					} else {
						phase = Integer.parseInt(objTemp.get(4).toString());
					}
					Patient tmpP = new Patient(Integer.parseInt(objTemp.get(0).toString()),objTemp.get(1).toString(), objTemp.get(2).toString(), age, phase, objTemp.get(5).toString(), objTemp.get(6).toString());
					new ModifPatient(3, tmpP, this);
				}
			}
			
		} else if (source == supprimer) { // The delete button
			if(stock.isSelected()) { //If the current table is the stock table
				int[] row = tabStock.getSelectedRows();
				if(tabStock.getSelectedRow() != -1) {
					Vector objTemp = modelStock.getDataVector().elementAt(tabStock.getSelectedRow()); //to get the string, use objTemp.get(i);
					modelStock.removeRow(tabStock.getSelectedRow());
					//TODO Delete the row from the database
					Stock s =stockDAO.getStock(Integer.parseInt(objTemp.get(0).toString()));
					stockDAO.supprimer(s);
					//Used to refresh the table
					updateStockTable();
				}
				
			} else if(patients.isSelected()) { //If the current table is the patient table
				int[] row = tabPatients.getSelectedRows();
				if(tabPatients.getSelectedRow() != -1) {
					Vector objTemp = modelPatients.getDataVector().elementAt(tabPatients.getSelectedRow()); //to get the string, use objTemp.get(i);
					modelPatients.removeRow(tabPatients.getSelectedRow());
					//TODO Delete the row from the database
					Patient p=patientDAO.getPatient(Integer.parseInt(objTemp.get(0).toString()));
					patientDAO.supprimer(p);
					//Used to refresh the table
					updatePatientTable();
				}
			}
		}
	}
	
	/**
	 * Permet de mettre à jour les données de la table de stocks
	 */
	public void updateStockTable() {
		//Update of the stock table
		//Only need to replace the tableData.data's values with the one from the database
		//Can add paramter to the function to bring data from another class, like a DAO
		//Even if the columnName don't change, keep that line to avoid the columnName being the one from the patient table
		tableData.columnName = new String[]{"Numero", "Fournisseur", "Nom du vaccin", "Date d'entree", "Nombre de stock"};
		tableData.data= new Object[stockDAO.getListeStocks().size()][];
		int i=0;
		for(Stock s : stockDAO.getListeStocks())
		{
			Object[] data = new Object[5];
			data[0]=s.getNumero();
			data[1]=s.getNomFournisseur();
			data[2]=s.getNomVaccin();
			data[3]=String.valueOf(s.getDateEntree());
			data[4]=s.getNombre();
			tableData.data[i]=data;
			i++;
		}
		modelStock = new DefaultTableModel(tableData.data, tableData.columnName);
		tabStock = new JTable(modelStock) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		modelStock.fireTableDataChanged();
		scrollPane.setViewportView(tabStock);
		sorterStock = new TableRowSorter<TableModel>(tabStock.getModel());
		sorterStock.setRowFilter(RowFilter.regexFilter(tableData.filter, 0, 1));
		tabStock.setRowSorter(sorterStock);
	}
	
	/**
	 * Permet de mettre à jour la table des patients
	 */
	public void updatePatientTable() {
		//Update of the patient table
		//Only need to replace the tableData.data's values with the one from the database
		//Can add paramter to the function to bring data from another class, like a DAO
		//Even if the columnName don't change, keep that line to avoid the columnName being the one from the stock table
		tableData.columnName = new String[]{"Id", "Nom", "Prenom", "Age", "Phase", "Profession", "Medecin traitant", "Date d'injection"};
		Injection injection;
		tableData.data= new Object[patientDAO.getListePatients().size()][];
		int i=0;
		
		for(Patient p : patientDAO.getListePatients())
		{
			Object[] data = new Object[8];
			injection= injectionDAO.getInjection(p.getId());
			data[0]=p.getId();
			data[1]=p.getNom();
			data[2]=p.getPrenom();
			data[3]=p.getAge();
			data[4]=p.getPhase();
			data[5]=p.getProfession();
			data[6]=p.getNomMedecinTraitant();
			if(injection != null) {
				data[7]=injection.getDateInjection();
			}else {
				data[7]="";
			}
			tableData.data[i]=data;
			i++;
		}
		modelPatients = new DefaultTableModel(tableData.data, tableData.columnName);
		tabPatients = new JTable(modelPatients) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		//This part refresh the table and the scrollpane
		modelPatients.fireTableDataChanged();
		scrollPane.setViewportView(tabPatients);
		sorterPatients = new TableRowSorter<TableModel>(tabPatients.getModel());
		sorterPatients.setRowFilter(RowFilter.regexFilter(tableData.filter, 0, 1));
		tabPatients.setRowSorter(sorterPatients);
	}

	/**
	 * Permet d'ajouter une ligne dans la table des patients
	 * @param p
	 */
	public void addLineToPatient(Patient p) {
		//Put code to add new patient here
		modelPatients.addRow(new Object[] {p.getId(), p.getNom(), p.getPrenom(), p.getAge(), p.getPhase(), p.getProfession(), p.getNomMedecinTraitant(), "", ""});
		System.out.println("Added : " + p.getNom() + " " + p.getPrenom() );
		//This line needs to be at the end of the function since it refresh the table
		updatePatientTable();
	}
	
	/**
	 * Permet de modifier une ligne dans la table des patients
	 * @param p
	 */
	public void modifyLineToPatient(Patient p) {
		//Put code to modify a patient here
		int indexSelected = tabPatients.getSelectedRow(); //index of the currently selected row, should be useful
		modelPatients.addRow(new Object[] {p.getId(), p.getNom(), p.getPrenom(), p.getAge(), p.getPhase(), p.getProfession(), p.getNomMedecinTraitant(), "", ""});
		System.out.println("Modifyed : " + p.getNom() + " " + p.getPrenom() );
		//This line needs to be at the end of the function since it refresh the table
		updatePatientTable();
	}
	
	/**
	 * Permet d'ajouter une ligne dans la table des stocks
	 * @param s
	 */
	public void addLineToStock(Stock s) {
		//Put code to add new stock here
		modelStock.addRow(new Object[] {s.getNumero(), s.getNomFournisseur(), s.getNomVaccin(), s.getDateEntree().toString(), s.getNombre()});
		System.out.println("Added : " + s.getNomFournisseur() + " " + s.getNomVaccin());
		//This line needs to be at the end of the function since it refresh the table
		updateStockTable();
	}
	
	/**
	 * Permet de modifier une ligne dans la table des stocks
	 * @param s
	 */
	public void modifyLineToStock(Stock s) {
		//Put code to modify a stock here
		int indexSelected = tabStock.getSelectedRow(); //index of the currently selected row, should be useful
		modelStock.addRow(new Object[] {s.getNumero(), s.getNomFournisseur(), s.getNomVaccin(), s.getDateEntree().toString(), s.getNombre()});
		System.out.println("Modifyed : " + s.getNomFournisseur() + " " + s.getNomVaccin());
		//This line needs to be at the end of the function since it refresh the table
		updateStockTable();
	}

	
	public static void main(String[] args) throws ParseException
	{
		new GestionFenetre();
    }

}
