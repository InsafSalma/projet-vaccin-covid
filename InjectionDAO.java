package modules;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 * classe de connection entre la table injection et l'application
 */
public class InjectionDAO {
	final static String URL = "jdbc:mysql://localhost:3306/stockvaccincovid";
	final static String LOGIN="root";
	final static String PASS="";
	/**
	 * constructor
	 */
	public InjectionDAO()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}
	}
	/**
	 * ajoute une injection à la table de donnee
	 * @param nouvInjection nouvelle injection
	 * @return nombre d'execution
	 */
	public int ajouter(Injection nouvInjection)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion à la base de données
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("INSERT INTO injection (idpatient, NumeroStock, Date) VALUES ( ?, ?, ?)");
			ps.setInt(1,nouvInjection.getIdPatient());
			ps.setInt(2,nouvInjection.getNumeroStock());
			ps.setDate(3, new java.sql.Date(nouvInjection.getDateInjection().getTime()) );

			//Exécution de la requête
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	/**
	 * modifie l'injection choisit
	 * @param injection injection choisit
	 * @return nombre d'execution
	 */
	public int modifier(Injection injection)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion à la base de données
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("Update injection SET Date= ? where IdPatient = ? and numerostock= ?");
			ps.setDate(1,new java.sql.Date(injection.getDateInjection().getTime()));
			ps.setInt(2,injection.getIdPatient());
			ps.setInt(3,injection.getNumeroStock());
			//Exécution de la requête
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
/**
 * supprime l'injection choisit
 * @param injection injection choisit
 * @return nombre d'execution
 */
	public int supprimer(Injection injection)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion à la base de données
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("Delete FROM injection  where IdPatient = ? and numerostock= ?");
			ps.setInt(1,injection.getIdPatient());
			ps.setInt(2,injection.getNumeroStock());
			//Exécution de la requête
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	/**
	 * renvoie l'injection qui a l'iddu patient et le numero du stock passe en parametre 
	 * @param idpatient identifiant du patient à injecté
	 * @param numerostock numéro du stock injecté
	 * @return Injection trouve
	 */
	public Injection getInjection(int idpatient, int numerostock)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Injection retour=null;

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM injection  where IdPatient = ? and numerostock= ?");
			ps.setInt(1,idpatient);
			ps.setInt(2,numerostock);

			//on exécute la requête
			rs=ps.executeQuery();
			if(rs.next())
				retour=new Injection(rs.getInt("idPatient"),rs.getInt("numeroStock"),rs.getDate("Date"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	/**
	 * renvoie l'injection qui a l'id du patient passe en parametre 
	 * @param idpatient identifiant du patient à injecté
	 * @return Injection trouve
	 */
	public Injection getInjection(int idpatient)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Injection retour=null;

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM injection where IdPatient = ? LIMIT 1  ");
			ps.setInt(1,idpatient);

			//on exécute la requête
			rs=ps.executeQuery();
			if(rs.next())
				retour=new Injection(rs.getInt("idPatient"),rs.getInt("numeroStock"),rs.getDate("Date"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
/**
 * renvoie toutes les injections sur la base de donnee
 * @return liste d'injections
 */
	public List<Injection> getListeInjections()
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		List<Injection> retour=new ArrayList<Injection>();

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM injection");

			//on exécute la requête
			rs=ps.executeQuery();
			while(rs.next())
				retour.add(new Injection(rs.getInt("idPatient"),rs.getInt("numeroStock"),rs.getDate("Date")));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du rs, du preparedStatement et de la connexion
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
/*	public static void main(String[] args)  throws SQLException {

	 InjectionDAO DAO=new InjectionDAO();

	 Injection a = new Injection(1,1,Date.valueOf("2020-10-12"));
	int retour=DAO.ajouter(a);

	System.out.println(retour+ " lignes ajoutées");

	Injection a2 = DAO.getInjection(1,1);
	System.out.println(a2.toString());
	a2.setDateInjection(Date.valueOf("2020-12-12"));
	DAO.modifier(a2);
	Injection a3 = DAO.getInjection(1,1);
	System.out.println(a3.toString());
	
	
	List<Injection> liste=DAO.getListeInjections();
	//System.out.println(liste);
	for(Injection i : liste)
	{
		System.out.println(i.toString());
	}
	
	DAO.supprimer(a3);

}*/
}
