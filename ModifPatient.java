package modules;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.Dimension;
import java.awt.Window;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ListSelectionModel;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import modules.GestionFenetre.Temp;

import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.BoxLayout;
import javax.swing.Box;

import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Permet de cr�er la fenetre d'ajout ou de modification de patients
 *
 */
public class ModifPatient extends JFrame implements ActionListener
{
	/**
	 * numero de version pour classe serialisable
	 */
	private static final long serialVersionUID = 1L; 
	
	//Un conteneur
	private JPanel containerPanel;
	private JTextField tfId = new JTextField();
	private JTextField tfName = new JTextField();
	private JTextField tfSurname = new JTextField();
	private JTextField tfAge = new JTextField();
	private JTextField tfPhase = new JTextField();
	private JTextField tfProfession = new JTextField();
	private JTextField tfFirstInj = new JTextField();
	private JTextField tfSecondInj = new JTextField();
	private JTextField tfNameDoc = new JTextField();
	private JComboBox cbVaccin1;
	private JComboBox cbVaccin2;
	private JButton save, close;
	
	JPanel pId, pName, pSurname, pAge, pPhase, pProf, pFirst, pSecond, pNameDoc, pButtons;
	
	int stateWindow = -1; //This integer is used to determine whether the window is used to add, modify or just see the data of a patient
	
	public Patient getPatient() { return this.patient; }
	
	class Temp { public GestionFenetre gF; public int state = -1; public String button; public Patient patientData;}
	final Temp tmp = new Temp();
	
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	private Patient patient;
	private Injection injection1, injection2;
	private PatientDAO patientDAO;
	private InjectionDAO injectionDAO;
	private StockDAO stockDAO;
	private Date today= new Date();
	
	/**
	 * Constructeur
	 * Definit la fenetre et ses composants - affiche la fenetre
	 */
	public ModifPatient(int state, Patient patient, GestionFenetre gF)
    {
		tmp.gF = gF;
		tmp.state = state;
		stockDAO= new StockDAO();
		patientDAO = new PatientDAO();
		injectionDAO= new InjectionDAO();
		injection1= new Injection(0,0,today);
		injection2= new Injection(0,0,today);
		containerPanel = new JPanel();
		stateWindow = state;
		this.patient = patient;
		JLabel id = new JLabel();
		JLabel name = new JLabel();
		JLabel surname = new JLabel();
		JLabel age = new JLabel();
		JLabel phase = new JLabel();
		JLabel profession = new JLabel();
		JLabel firstInj = new JLabel();
		JLabel secondInj = new JLabel();
		JLabel nameDoc = new JLabel(); /*
		JTextField tfId = new JTextField();
		JTextField tfName = new JTextField();
		JTextField tfSurname = new JTextField();
		JTextField tfAge = new JTextField();
		JTextField tfPhase = new JTextField();
		JTextField tfProfession = new JTextField();
		JTextField tfFirstInj = new JTextField();
		JTextField tfSecondInj = new JTextField();
		JTextField tfNameDoc = new JTextField(); */
		save = new JButton("Enregistrer");
		close = new JButton("Close");
		save.addActionListener(this);
		close.addActionListener(this);
		
		id.setText("Id : ");
		name.setText("Nom : ");
		surname.setText("Prenom : ");
		age.setText("Age : ");
		phase.setText("Phase : ");
		profession.setText("Profession : ");
		firstInj.setText("Date premiere injection : ");
		secondInj.setText("Date deuxieme injection : ");
		nameDoc.setText("Nom medecin traitant : ");
		
		tfId.setText(Integer.toString(patient.getId()));
		tfName.setText(patient.getNom());
		tfSurname.setText(patient.getPrenom());
		tfAge.setText(Integer.toString(patient.getAge()));
		tfPhase.setText(Integer.toString(patient.getPhase()));
		tfProfession.setText(patient.getProfession());
		tfFirstInj.setText(df.format(today));
		tfSecondInj.setText(df.format(today));
		tfNameDoc.setText(patient.getNomMedecinTraitant());
		fill_comboBox();
		
		//Etat = 1 => Etat "Ajout";
		//Etat = 2 => Etat "Modification"
		//Etat = 3 => Etat "Obtenir plus d'information"
		tfId.setEnabled(false);
		tfPhase.setEnabled(false);
		if(stateWindow == 3) {
			tfName.setEnabled(false);
			tfSurname.setEnabled(false);
			tfAge.setEnabled(false);
			tfProfession.setEnabled(false);
			tfFirstInj.setEnabled(false);
			tfSecondInj.setEnabled(false);
			tfNameDoc.setEnabled(false);
			save.setEnabled(false);
		}
		
		
		Dimension boxDimPass = tfId.getMinimumSize(); boxDimPass.setSize(boxDimPass.getWidth()+800, boxDimPass.getHeight());
		tfId.setMaximumSize(boxDimPass);
		tfName.setMaximumSize(boxDimPass);
		tfSurname.setMaximumSize(boxDimPass);
		tfAge.setMaximumSize(boxDimPass);
		tfPhase.setMaximumSize(boxDimPass);
		tfProfession.setMaximumSize(boxDimPass);
		tfFirstInj.setMaximumSize(boxDimPass);
		tfSecondInj.setMaximumSize(boxDimPass);
		tfNameDoc.setMaximumSize(boxDimPass);
		Dimension boxDimCB = cbVaccin1.getMinimumSize(); boxDimCB.setSize(boxDimCB.getWidth()+800, boxDimCB.getHeight());
		cbVaccin1.setMaximumSize(boxDimCB);
		cbVaccin2.setMaximumSize(boxDimCB);
		
		pId = new JPanel();
		pName = new JPanel();
		pSurname = new JPanel();
		pAge = new JPanel();
		pPhase = new JPanel();
		pProf = new JPanel();
		pFirst = new JPanel();
		pSecond = new JPanel();
		pNameDoc = new JPanel();
		pButtons = new JPanel();
		
		pId.setLayout(new BoxLayout(pId, BoxLayout.LINE_AXIS));
		pName.setLayout(new BoxLayout(pName, BoxLayout.LINE_AXIS));
		pSurname.setLayout(new BoxLayout(pSurname, BoxLayout.LINE_AXIS));
		pAge.setLayout(new BoxLayout(pAge, BoxLayout.LINE_AXIS));
		pPhase.setLayout(new BoxLayout(pPhase, BoxLayout.LINE_AXIS));
		pProf.setLayout(new BoxLayout(pProf, BoxLayout.LINE_AXIS));
		pFirst.setLayout(new BoxLayout(pFirst, BoxLayout.LINE_AXIS));
		pSecond.setLayout(new BoxLayout(pSecond, BoxLayout.LINE_AXIS));
		pNameDoc.setLayout(new BoxLayout(pNameDoc, BoxLayout.LINE_AXIS));
		pButtons.setLayout(new BoxLayout(pButtons, BoxLayout.LINE_AXIS));
		
		pId.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		pId.add(id);
		pId.add(Box.createRigidArea(new Dimension(10,0)));
		pId.add(tfId);
		
		pName.add(name);
		pName.add(Box.createRigidArea(new Dimension(10,0)));
		pName.add(tfName);
		
		pSurname.add(surname);
		pSurname.add(Box.createRigidArea(new Dimension(10,0)));
		pSurname.add(tfSurname);
		
		pAge.add(age);
		pAge.add(Box.createRigidArea(new Dimension(10,0)));
		pAge.add(tfAge);
		
		pPhase.add(phase);
		pPhase.add(Box.createRigidArea(new Dimension(10,0)));
		pPhase.add(tfPhase);
		
		pProf.add(profession);
		pProf.add(Box.createRigidArea(new Dimension(10,0)));
		pProf.add(tfProfession);
		
		pFirst.add(firstInj);
		pFirst.add(Box.createRigidArea(new Dimension(10,0)));
		pFirst.add(tfFirstInj);
		pFirst.add(Box.createRigidArea(new Dimension(10,0)));
		pFirst.add(cbVaccin1);
		
		pSecond.add(secondInj);
		pSecond.add(Box.createRigidArea(new Dimension(10,0)));
		pSecond.add(tfSecondInj);
		pSecond.add(Box.createRigidArea(new Dimension(10,0)));
		pSecond.add(cbVaccin2);
		
		pNameDoc.add(nameDoc);
		pNameDoc.add(Box.createRigidArea(new Dimension(10,0)));
		pNameDoc.add(tfNameDoc);
		
		pButtons.add(Box.createRigidArea(new Dimension(10,0)));
		pButtons.add(save);
		pButtons.add(Box.createRigidArea(new Dimension(10,0)));
		pButtons.add(close);
		pButtons.add(Box.createRigidArea(new Dimension(10,0)));
		
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));
		containerPanel.add(Box.createRigidArea(new Dimension(0,50)));
		containerPanel.add(pId);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pName);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pSurname);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pAge);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pPhase);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pProf);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pFirst);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pSecond);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pNameDoc);
		containerPanel.add(Box.createRigidArea(new Dimension(0,25)));
		containerPanel.add(pButtons);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setContentPane(containerPanel);
		this.setTitle("Ajouter/modifier/consulter un patient");
		this.setSize(new Dimension(1000,550));
		this.setVisible(true);
		
		WindowListener exitListener = new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
            	if(tmp.button == "save") {
            		if(tmp.state == 1) {
                		tmp.gF.addLineToPatient(tmp.patientData);
                		System.out.println("Adding patient ...");
                		
                	} else if (tmp.state == 2) {
                		tmp.gF.modifyLineToPatient(tmp.patientData);
                		System.out.println("Modifying patient ...");
                	}
            	} else {
            		System.out.println("Saving without closing ...") ;
            	}
            }
        };
        this.addWindowListener(exitListener);
	}
	
	/**
	 * Gere les actions realisees par les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae)
	{
		Object source = ae.getSource();
		if(source == save) {
			tmp.button = "save";
			int age;
			String stringAge = tfAge.getText();
			if("".contains(stringAge)) {
				age = 0;
			} else {
				age = Integer.parseInt(tfAge.getText());
			}
			int phase;
			if("".contains(tfPhase.getText())) {
				phase = 0;
			} else {
				phase = Integer.parseInt(tfPhase.getText());
			}
			tmp.patientData = new Patient(this.tfName.getText(), this.tfSurname.getText(), age, phase, tfProfession.getText(), tfNameDoc.getText());
			Window window = SwingUtilities.windowForComponent((JButton)ae.getSource());
	        window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
	        if(stateWindow == 1) {
				//Todo : ajout du patient
	        	try {
	        	modify_patient();
	        	patientDAO.ajouter(patient);
	        	patient= patientDAO.getPatient(patient.getNom(),patient.getPrenom());
	        	modify_injection();
	        	injectionDAO.ajouter(injection1);
	        	injectionDAO.ajouter(injection2);
	        	clear_fields();
	        	tmp.gF.updatePatientTable();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	        	
			} else if (stateWindow == 2) {
				//Todo : modification du patient
	        	try {
	        	modify_patient();
	        	patientDAO.modifier(patient);
	        	patient= patientDAO.getPatient(patient.getId());
	        	modify_injection();
	        	injectionDAO.modifier(injection1);
	        	injectionDAO.modifier(injection2);
	        	clear_fields();
	        	tmp.gF.updatePatientTable();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
		} else if(source == close) {
			Window window = SwingUtilities.windowForComponent((JButton)ae.getSource());
	        window.dispatchEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSING));
			tmp.button = "close";
		}
	}
	
	/**
	 * Permet de remplir les combo box avec des donn�es de vaccins de la base de donn�es
	 */
	public void fill_comboBox()
	{
		String[] list= new String[stockDAO.getListeStocks().size()]; 
        int i=0;
        for(Stock s : stockDAO.getListeStocks())
        {
            list[i]=s.getNomVaccin();
            i++;
        }
        cbVaccin1= new JComboBox(list);
        cbVaccin2= new JComboBox(list);
	}
	
	
	public void modify_patient()
	{
		patient.setNom(tfName.getText()) ;
		patient.setPrenom(tfSurname.getText());
		patient.setAge(Integer.parseInt(tfAge.getText()));
		patient.setProfession(tfProfession.getText());
		patient.setNomMedecinTraitant(tfNameDoc.getText());
		//generer le numero de phase lol with what tho 
		
		///
		
	}
	public void modify_injection() throws ParseException
	{
		String target = this.tfFirstInj.getText();
		Date date1= df.parse(target);
		String target2 = this.tfFirstInj.getText();
		Date date2= df.parse(target2);
		int numVaccin1,numVaccin2;
		//numVaccin1 = stockDAO.get_index(vaccin1.getSelectedItem().toString());
		//numVaccin2 = stockDAO.get_index(vaccin1.getSelectedItem().toString());
		injection1.setDateInjection(date1);
		//injection1.setNumeroStock(numVaccin1);
		injection2.setDateInjection(date2);
		//injection2.setNumeroStock(numVaccin2);
		injection1.setIdPatient(patient.getId());
		injection2.setIdPatient(patient.getId());
		injection2.setNumeroStock(stockDAO.get_index(cbVaccin2.getSelectedItem().toString()));
		injection1.setNumeroStock(stockDAO.get_index(cbVaccin1.getSelectedItem().toString()));
	}
	
	/**
	 * Permet de vider les champs textes pr�sents sur la page
	 */
	public void clear_fields()
	{
		tfId.setText("");
		tfName.setText("");
		tfSurname.setText("");
		tfAge.setText("");
		tfPhase.setText("");
		tfProfession.setText(patient.getProfession());
		tfFirstInj.setText("");
		tfSecondInj.setText("");
		tfNameDoc.setText(patient.getNomMedecinTraitant());
	}
	
	public static void main(String[] args)
	{
		//new ModifPatient(1, new Patient("Jean", "Suismalade", 42, 2, "Enseignant", "Jean Soigne"));
    }

}
