package modules;
import java.util.Date;

public class Injection {

	private int idPatient;
	private int numeroStock;
	private Date dateInjection;
	public Injection(int idPatient, int numeroStock, Date dateInjection) {
		super();
		this.idPatient = idPatient;
		this.numeroStock = numeroStock;
		this.dateInjection = dateInjection;
	}
	public int getIdPatient() {
		return idPatient;
	}
	public void setIdPatient(int idPatient) {
		this.idPatient = idPatient;
	}
	public int getNumeroStock() {
		return numeroStock;
	}
	public void setNumeroStock(int numeroStock) {
		this.numeroStock = numeroStock;
	}
	public Date getDateInjection() {
		return dateInjection;
	}
	public void setDateInjection(Date dateInjection) {
		this.dateInjection = dateInjection;
	}
	@Override
	public String toString() {
		return "Injection [idPatient=" + idPatient + ", numeroStock=" + numeroStock + ", dateInjection=" + dateInjection
				+ "]";
	}
}
