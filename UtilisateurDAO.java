package modules;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 * Classe de laconnection entre la table Utilisateur et la base de donnee
 */
public class UtilisateurDAO {
	final static String URL = "jdbc:mysql://localhost:3306/stockvaccincovid";
	final static String LOGIN="root";
	final static String PASS="";
	/**
	 * constructor
	 */
	public UtilisateurDAO()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}

	}
	/**
	 * ajouter utilisateur à la base de donnee
	 * @param nouvUtilisateur nouveau utilisateur
	 * @return nombre d'execution
	 */
	public int ajouter(Utilisateur nouvUtilisateur)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("INSERT INTO utilisateur (Nom,Prenom,NumTel,Adresse,Mail,MotDePasse) VALUES ( ?, ?, ?, ?, ?, ?)");
			ps.setString(1,nouvUtilisateur.getNom());
			ps.setString(2,nouvUtilisateur.getPrenom());
			ps.setString(3,nouvUtilisateur.getNumTelephone());
			ps.setString(4,nouvUtilisateur.getAdresse());
			ps.setString(5,nouvUtilisateur.getMail());
			ps.setString(6,nouvUtilisateur.getMotDePasse());

			//Exécution de la requête
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	/**
	 * renvoie utilisateur qui a le mail et le mot de passe passe en parametre
	 * @param mail e-mail
	 * @param motDePasse mot de passe
	 * @return utilisateur trouve
	 */
	
	public Utilisateur getUtilisateur(String mail, String motDePasse)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Utilisateur retour=null;

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Utilisateur WHERE mail = ? and motDePasse = ?");
			ps.setString(1,mail);
			ps.setString(2,motDePasse);
			//on exécute la requête
			//rs contient un pointeur situé jusute avant la première ligne retournée
			rs=ps.executeQuery();
			//passe à la première (et unique) ligne retournée 
			if(rs.next())
				retour=new Utilisateur(rs.getString("Nom"),rs.getString("Prenom"),rs.getString("NumTel"),rs.getString("Adresse"),rs.getString("Mail"),rs.getString("MotDePasse"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
/*	public static void main(String[] args)  throws SQLException {

		 UtilisateurDAO DAO=new UtilisateurDAO();
		//test de la méthode ajouter
		Utilisateur a = new Utilisateur("nom","Prenom","NumTel","Adresse","Mail","MotDePasse");
		int retour=DAO.ajouter(a);

		System.out.println(retour+ " lignes ajoutées");

		//test de la méthode getArticle
		Utilisateur a2 =DAO.getUtilisateur("Mail","MotDePasse");
		System.out.println(a2.getMail());
		System.out.println(a2.getMotDePasse());

	}*/

}
