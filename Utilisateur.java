package modules;

public class Utilisateur {

	private int id;
	private String nom;
	private String prenom;
	private String numTelephone;
	private String adresse;
	private String mail;
	private String motDePasse;
	public Utilisateur(String nom, String prenom, String numTelephone, String adresse, String mail, String motDePasse) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.numTelephone = numTelephone;
		this.adresse = adresse;
		this.mail = mail;
		this.motDePasse = motDePasse;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNumTelephone() {
		return numTelephone;
	}
	public void setNumTelephone(String numTelephone) {
		this.numTelephone = numTelephone;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getMotDePasse() {
		return motDePasse;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
