package modules;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 * Classe de connection entre la table stock et l'application
 */
public class StockDAO {
	final static String URL = "jdbc:mysql://localhost:3306/stockvaccincovid";
	final static String LOGIN="root";
	final static String PASS="";
	/**
	 * Constructor
	 */
	public StockDAO()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}
	}
	
	/**
	 * Ajouter un stock à la base de donnée
	 * @param nouvVaccin nouveau vaccin
	 * @return nombre d'execution
	 */
	public int ajouter(Stock nouvVaccin)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion à la base de données
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("INSERT INTO stock (NomFournisseur,NomVaccin,DateEntree, Nombre) VALUES ( ?, ?, ?, ?)");
			ps.setString(1,nouvVaccin.getNomFournisseur());
			ps.setString(2,nouvVaccin.getNomVaccin());
			java.sql.Date d= new java.sql.Date(nouvVaccin.getDateEntree().getTime());
			ps.setDate(3,d);
			ps.setInt(4,nouvVaccin.getNombre());

			//Exécution de la requête
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	
	/**
	 * Modifier un stock à la base de donnée
	 * @param Vaccin vaccin choisit par l'utilisateur
	 * @return nombre d'execution
	 */
	public int modifier(Stock Vaccin)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion à la base de données
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("Update stock SET NomFournisseur= ? ,NomVaccin = ?,DateEntree= ?, Nombre= ? where Numero = ? ");
			ps.setString(1,Vaccin.getNomFournisseur());
			ps.setString(2,Vaccin.getNomVaccin());
			ps.setDate(3,new java.sql.Date(Vaccin.getDateEntree().getTime()));
			ps.setInt(4,Vaccin.getNombre());
			ps.setInt(5,Vaccin.getNumero());
			//Exécution de la requête
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	/**
	 * Supprimer un stock à la base de donnée
	 * @param Vaccin vaccin choisit par l'utilisateur
	 * @return nombre d'execution
	 */
	public int supprimer(Stock Vaccin)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion à la base de données
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("Delete FROM stock  where Numero = ? ");
			ps.setInt(1,Vaccin.getNumero());
			//Exécution de la requête
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	
/**
 * getStock
 * @param Numero l'identifient du stock
 * @return renvoie le stock du numero passe en parametre
 */
	public Stock getStock(int Numero)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Stock retour=null;

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM stock WHERE Numero = ?");
			ps.setInt(1,Numero);

			//on exécute la requête
			rs=ps.executeQuery();
			if(rs.next())
				retour=new Stock(rs.getInt("Numero"),rs.getString("NomFournisseur"),rs.getString("NomVaccin"),rs.getDate("DateEntree"),rs.getInt("Nombre"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
/**
 * get_index
 * @param nomVaccin Le nom du vaccin choisit sur la liste deroulante
 * @return renvoie le numero du vaccin/stock
 */
	public int get_index(String nomVaccin)
	{
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		int retour=0;

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT Numero FROM stock WHERE Nomvaccin = ?");
			ps.setString(1,nomVaccin);

			//on exécute la requête
			rs=ps.executeQuery();
			if(rs.next())
				retour=rs.getInt("Numero");

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;
	}
	/**
	 * getListeStocks
	 * @return renvoie la liste des stocks stockee dans la base de donnée
	 */
	public List<Stock> getListeStocks()
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		List<Stock> retour=new ArrayList<Stock>();

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM stock");

			//on exécute la requête
			rs=ps.executeQuery();
			while(rs.next())
				retour.add(new Stock(rs.getInt("Numero"),rs.getString("NomFournisseur"),rs.getString("NomVaccin"),rs.getDate("DateEntree"),rs.getInt("Nombre")));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du rs, du preparedStatement et de la connexion
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
/*	public static void main(String[] args)  throws SQLException {

		 StockDAO DAO=new StockDAO();

		Stock a = new Stock("me","vaccin1",Date.valueOf("2020-12-12"),6);
		int retour=DAO.ajouter(a);

		System.out.println(retour+ " lignes ajoutées");

		Stock a2 = DAO.getStock(2);
		System.out.println(a2.toString());
		a2.setNomVaccin("vaccin2");
		
		DAO.modifier(a2);
		Stock a3 = DAO.getStock(2);
		System.out.println(a3.toString());
		
		
		List<Stock> liste=DAO.getListeStocks();
		//System.out.println(liste);
		for(Stock stock : liste)
		{
			System.out.println(stock.toString());
		}
		

	}*/

}
