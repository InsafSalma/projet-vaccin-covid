package modules;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.BoxLayout;
import javax.swing.Box;
import java.util.List;

/**
 * Permet de cr�er la fen�tre de connexion
 *
 */
public class LoginFenetre extends JFrame implements ActionListener
{
	/**
	 * numero de version pour classe serialisable
	 */
	private static final long serialVersionUID = 1L; 
	
	//Un conteneur
	private JPanel containerPanel;		
	private JLabel catTitle, auth, pass;
	private JTextField authentification;
	private JPasswordField password;
	private JButton co;
	private JButton insc;
	//Right part of the screen
	private JTextField creaName2, creaSurname2, creaTelNum2, creaMail2, creaAdd2;
	//Left part of the screen
	private JPasswordField creaPass2, creaPassVer2;
	private UtilisateurDAO utilisateurDAO;
	
	/**
	 * Constructeur
	 * D�finis la fen�tre et affiche ses composants
	 */
	public LoginFenetre()
    {
		//instencier la classe dao
		utilisateurDAO= new UtilisateurDAO();
		
		containerPanel = new JPanel();
		
		//All of this is only for the left part of the login screen. We do sure love having to use complexe code while gui exist to do this instead !
		
		
		catTitle = new JLabel();
		catTitle.setText("Vous etes deja inscrit");
		catTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
		auth = new JLabel();
		auth.setText("mail : ");
		pass = new JLabel();
		pass.setText("Mot de passe : ");
		
		authentification = new JTextField();
		password = new JPasswordField();
		Dimension boxDimPass = password.getMinimumSize(); boxDimPass.setSize(boxDimPass.getWidth()+1000, boxDimPass.getHeight());
		password.setMaximumSize(boxDimPass);
		Dimension boxDimAuth = authentification.getMinimumSize(); boxDimAuth.setSize(boxDimAuth.getWidth()+800, boxDimAuth.getHeight());
		authentification.setMaximumSize(boxDimAuth);
		
		//Button
		co = new JButton("Connection");
		co.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JPanel panelAuth = new JPanel();
		JPanel panelPass = new JPanel();
		panelAuth.setLayout(new BoxLayout(panelAuth, BoxLayout.LINE_AXIS));
		panelPass.setLayout(new BoxLayout(panelPass, BoxLayout.LINE_AXIS));
		panelAuth.add(auth);
		panelAuth.add(Box.createRigidArea(new Dimension(10,0)));
		panelAuth.add(authentification);
		panelPass.add(pass);
		panelPass.add(Box.createRigidArea(new Dimension(10,0)));
		panelPass.add(password);
		
		JPanel panelLeft = new JPanel();
		panelLeft.setLayout(new BoxLayout(panelLeft, BoxLayout.PAGE_AXIS));
		panelLeft.add(catTitle);
		panelLeft.add(Box.createRigidArea(new Dimension(0,50)));
		panelLeft.add(panelAuth);
		panelLeft.add(Box.createRigidArea(new Dimension(0,5)));
		panelLeft.add(panelPass);
		panelLeft.add(Box.createRigidArea(new Dimension(0,10)));
		panelLeft.add(co);
		//End of the left part of the login screen
		
		//Right part of the screen
		JLabel creaAccount, creaName, creaSurname, creaTelNum, creaMail, creaPass, creaPassVer, creaAdd;
		//Initialisation of JLabel -_-
		creaAccount = new JLabel();
		creaAccount.setText("Vous n'etes pas un membre");
		creaName = new JLabel();
		creaName.setText("Nom :");
		creaSurname = new JLabel();
		creaSurname.setText("Prenom : ");
		creaTelNum = new JLabel();
		creaTelNum.setText("Numero de tel : ");
		creaMail = new JLabel();
		creaMail.setText("Mail :");
		creaPass = new JLabel();
		creaPass.setText("Mot de passe : ");
		creaPassVer = new JLabel();
		creaPassVer.setText("Verification mdp : ");
		creaAdd = new JLabel();
		creaAdd.setText("Adresse : ");
		
		//Another initialisation
		creaName2 = new JTextField();
		creaSurname2 = new JTextField();
		creaTelNum2 = new JTextField();
		creaMail2 = new JTextField();
		creaAdd2 = new JTextField();
		
		//And again
		creaPass2 = new JPasswordField();
		creaPassVer2 = new JPasswordField();
		
		//Dimension of right part
		Dimension boxDimAuth2 = creaName2.getMinimumSize(); boxDimAuth2.setSize(creaName2.getWidth()+1000, creaName2.getHeight());
		creaPass2.setMaximumSize(boxDimPass);
		creaPassVer2.setMaximumSize(boxDimPass);
		Dimension boxDimPass2 = creaPass2.getMinimumSize(); boxDimPass2.setSize(creaPass2.getWidth()+800, creaPass2.getHeight());
		creaName2.setMaximumSize(boxDimAuth);
		creaSurname2.setMaximumSize(boxDimAuth);
		creaTelNum2.setMaximumSize(boxDimAuth);
		creaMail2.setMaximumSize(boxDimAuth);
		creaAdd2.setMaximumSize(boxDimAuth);
		
		//Now to add all of this into boxlayout for each line
		JPanel panelName = new JPanel();
		JPanel panelSurname = new JPanel();
		JPanel panelTel = new JPanel();
		JPanel panelMail = new JPanel();
		JPanel panelPassRight = new JPanel();
		JPanel panelPassVer = new JPanel();
		JPanel panelAddr = new JPanel();
		panelName.setLayout(new BoxLayout(panelName, BoxLayout.LINE_AXIS));
		panelSurname.setLayout(new BoxLayout(panelSurname, BoxLayout.LINE_AXIS));
		panelTel.setLayout(new BoxLayout(panelTel, BoxLayout.LINE_AXIS));
		panelMail.setLayout(new BoxLayout(panelMail, BoxLayout.LINE_AXIS));
		panelPassRight.setLayout(new BoxLayout(panelPassRight, BoxLayout.LINE_AXIS));
		panelPassVer.setLayout(new BoxLayout(panelPassVer, BoxLayout.LINE_AXIS));
		panelAddr.setLayout(new BoxLayout(panelAddr, BoxLayout.LINE_AXIS));
		
		panelName.add(creaName);
		panelName.add(Box.createRigidArea(new Dimension(10,0)));
		panelName.add(creaName2);
		panelSurname.add(creaSurname);
		panelSurname.add(Box.createRigidArea(new Dimension(10,0)));
		panelSurname.add(creaSurname2);
		panelTel.add(creaTelNum);
		panelTel.add(Box.createRigidArea(new Dimension(10,0)));
		panelTel.add(creaTelNum2);
		panelMail.add(creaMail);
		panelMail.add(Box.createRigidArea(new Dimension(10,0)));
		panelMail.add(creaMail2);
		panelPassRight.add(creaPass);
		panelPassRight.add(Box.createRigidArea(new Dimension(10,0)));
		panelPassRight.add(creaPass2);
		panelPassVer.add(creaPassVer);
		panelPassVer.add(Box.createRigidArea(new Dimension(10,0)));
		panelPassVer.add(creaPassVer2);
		panelAddr.add(creaAdd);
		panelAddr.add(Box.createRigidArea(new Dimension(10,0)));
		panelAddr.add(creaAdd2);
		
		//Button for the right part
		insc = new JButton("S'inscrire");
		insc.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JPanel panelRight = new JPanel();
		panelRight.setLayout(new BoxLayout(panelRight, BoxLayout.PAGE_AXIS));
		panelRight.add(creaAccount);
		panelRight.add(Box.createRigidArea(new Dimension(0,50)));
		panelRight.add(panelName);
		panelRight.add(Box.createRigidArea(new Dimension(0,5)));
		panelRight.add(panelSurname);
		panelRight.add(Box.createRigidArea(new Dimension(0,5)));
		panelRight.add(panelTel);
		panelRight.add(Box.createRigidArea(new Dimension(0,5)));
		panelRight.add(panelMail);
		panelRight.add(Box.createRigidArea(new Dimension(0,5)));
		panelRight.add(panelPassRight);
		panelRight.add(Box.createRigidArea(new Dimension(0,5)));
		panelRight.add(panelPassVer);
		panelRight.add(Box.createRigidArea(new Dimension(0,5)));
		panelRight.add(panelAddr);
		panelRight.add(Box.createRigidArea(new Dimension(0,10)));
		panelRight.add(insc);
		//End of the right part
		
		
		//Main container and settings
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.LINE_AXIS));
		containerPanel.add(Box.createRigidArea(new Dimension(50,0)));
		containerPanel.add(panelLeft);
		containerPanel.add(Box.createRigidArea(new Dimension(100,0)));
		containerPanel.add(panelRight);
		containerPanel.add(Box.createRigidArea(new Dimension(50,0)));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(containerPanel);
		this.setTitle("Authentification");
		this.setSize(new Dimension(1000,500));
		this.setVisible(true);
		//ajout des écouteurs sur les boutons pour gérer les évènements
		insc.addActionListener(this);
		co.addActionListener(this);
	}
	
	/**
	 * G�re les actions r�alis�es par les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae)
	{
		int retour; // code de retour de la classe UtilisateurDAO
		
		try {
			if(ae.getSource()==insc)
			{
				if(String.valueOf(this.creaPass2.getPassword()).equals(String.valueOf(this.creaPassVer2.getPassword())))
				{
					//on crée l'objet message
					Utilisateur u=new Utilisateur(this.creaName2.getText(),this.creaSurname2.getText(),this.creaTelNum2.getText(),this.creaAdd2.getText(),this.creaMail2.getText(),String.valueOf(this.creaPass2.getPassword()));
					//on demande à la classe de communication d'ajouter l'utilisateur à la base de donnée
					retour = utilisateurDAO.ajouter(u);

					System.out.println(" Utilisateur enregistré! ");
					//clearing the fields 
					this.creaName2.setText("");
					this.creaSurname2.setText("");
					this.creaTelNum2.setText("");
					this.creaAdd2.setText("");
					this.creaMail2.setText("");
					this.creaPass2.setText("");
					this.creaPassVer2.setText("");
				}else {
					//pop up message passwords not identical
					System.out.println(" Mot de passe de confirmation différent! ");
				}
			}
			else if(ae.getSource()==co)
			{
				Utilisateur utilisateur = utilisateurDAO.getUtilisateur(this.authentification.getText(),String.valueOf(this.password.getPassword()));
				if(utilisateur == null)
				{
					//pop up message mail ou mdp incorrecte ou vous n'etes pas encore inscrit
					System.out.println(" mail ou mdp incorrecte ou vous n'etes pas encore inscrit! ");
				}else {
					//temporary
					System.out.println(" Welcome");
					this.dispose();
					new GestionFenetre();
					//code to open the next window and close this one 
				}
			}
		}
		catch (Exception e) {
			System.err.println("Veuillez controler vos saisies");
		}
	}

	
	public static void main(String[] args)
	{
		new LoginFenetre();
    }

}
