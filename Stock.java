package modules;
import java.util.Date;

public class Stock {

	private int numero;
	private String nomFournisseur;
	private String nomVaccin;
	private Date dateEntree;
	private int nombre;
	public Stock(String nomFournisseur, String nomVaccin, Date dateEntree, int nombre) {
		super();
		this.nomFournisseur = nomFournisseur;
		this.nomVaccin = nomVaccin;
		this.dateEntree = dateEntree;
		this.nombre = nombre;
	}
	public Stock(int numero, String nomFournisseur, String nomVaccin, Date dateEntree, int nombre) {
		super();
		this.numero = numero;
		this.nomFournisseur = nomFournisseur;
		this.nomVaccin = nomVaccin;
		this.dateEntree = dateEntree;
		this.nombre = nombre;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getNomFournisseur() {
		return nomFournisseur;
	}
	public void setNomFournisseur(String nomFournisseur) {
		this.nomFournisseur = nomFournisseur;
	}
	public String getNomVaccin() {
		return nomVaccin;
	}
	public void setNomVaccin(String nomVaccin) {
		this.nomVaccin = nomVaccin;
	}
	public Date getDateEntree() {
		return dateEntree;
	}
	public void setDateEntree(Date dateEntree) {
		this.dateEntree = dateEntree;
	}
	public int getNombre() {
		return nombre;
	}
	public void setNombre(int nombre) {
		this.nombre = nombre;
	}
	@Override
	public String toString() {
		return "Stock [numero=" + numero + ", nomFournisseur=" + nomFournisseur + ", nomVaccin=" + nomVaccin
				+ ", dateEntree=" + dateEntree + ", nombre=" + nombre + "]";
	}
	
	
}
