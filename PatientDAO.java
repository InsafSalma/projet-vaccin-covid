package modules;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 * Classe de connection entre la table Patient et l'application
 */
public class PatientDAO {
	final static String URL = "jdbc:mysql://localhost:3306/stockvaccincovid";
	final static String LOGIN="root";
	final static String PASS="";
	/**
	 * constructor
	 */
	public PatientDAO()
	{
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e2) {
			System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
		}
	}
	/**
	 * ajouter un patient à la base de donnée
	 * @param nouvPatient nouveau patient 
	 * @return nombre d'execution
	 */
	public int ajouter(Patient nouvPatient)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion à la base de données
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("INSERT INTO patient (Nom, Prenom, Age, Phase, Profession, MedecinTraitant) VALUES ( ?, ?, ?, ?, ?, ?)");
			ps.setString(1,nouvPatient.getNom());
			ps.setString(2,nouvPatient.getPrenom());
			ps.setInt(3,nouvPatient.getAge());
			ps.setInt(4,nouvPatient.getPhase());
			ps.setString(5,nouvPatient.getProfession());
			ps.setString(6,nouvPatient.getNomMedecinTraitant());

			//Exécution de la requête
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	/**
	 * modifier un patient
	 * @param patient patient choisit 
	 * @return nombre d'execution
	 */
	public int modifier(Patient patient)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion à la base de données
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("Update PATIENT SET Nom= ? ,Prenom = ?,Age= ?, Phase= ?, Profession= ?, MedecinTraitant= ? where Id = ? ");
			ps.setString(1,patient.getNom());
			ps.setString(2,patient.getPrenom());
			ps.setInt(3,patient.getAge());
			ps.setInt(4,patient.getPhase());
			ps.setString(5,patient.getProfession());
			ps.setString(6,patient.getNomMedecinTraitant());
			ps.setInt(7,patient.getId());
			//Exécution de la requête
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	/**
	 * supprimer un patient
	 * @param patient patient choisit
	 * @return nombre d'execution
	 */
	public int supprimer(Patient patient)
	{
		Connection con = null;
		PreparedStatement ps = null;
		int retour=0;

		//connexion à la base de données
		try {

			//tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("Delete FROM patient  where Id = ? ");
			ps.setInt(1,patient.getId());
			//Exécution de la requête
			retour=ps.executeUpdate();


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du preparedStatement et de la connexion
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	/**
	 * renvoie le patient dont l'identifiant est passe en parametre
	 * @param id identifiant du patient
	 * @return patient trouvé
	 */
	public Patient getPatient(int id)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Patient retour=null;

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM patient WHERE id = ?");
			ps.setInt(1,id);

			//on exécute la requête
			rs=ps.executeQuery();
			if(rs.next())
				retour=new Patient(rs.getInt("id"),rs.getString("Nom"),rs.getString("Prenom"),rs.getInt("Age"),rs.getInt("Phase"),rs.getString("Profession"),rs.getString("MedecinTraitant"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
/**
 * renvoie le patient dont le nom et prenom sont passes en parametre
 * @param nom nom du patient
 * @param prenom prenom du patient 
 * @return le patient trouve
 */
	public Patient getPatient(String nom, String prenom)
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		Patient retour=null;

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM patient WHERE Nom= ? And Prenom = ?");
			ps.setString(1,nom);
			ps.setString(2, prenom);

			//on exécute la requête
			rs=ps.executeQuery();
			if(rs.next())
				retour=new Patient(rs.getInt("id"),rs.getString("Nom"),rs.getString("Prenom"),rs.getInt("Age"),rs.getInt("Phase"),rs.getString("Profession"),rs.getString("MedecinTraitant"));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du ResultSet, du PreparedStatement et de la Connection
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	/**
	 * renvoie la liste des patients sur la base de donnee
	 * @return la liste des patients
	 */
	public List<Patient> getListePatients()
	{

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs=null;
		List<Patient> retour=new ArrayList<Patient>();

		//connexion à la base de données
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM patient");

			//on exécute la requête
			rs=ps.executeQuery();
			while(rs.next())
				retour.add(new Patient(rs.getInt("id"),rs.getString("Nom"),rs.getString("Prenom"),rs.getInt("Age"),rs.getInt("Phase"),rs.getString("Profession"),rs.getString("MedecinTraitant")));


		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			//fermeture du rs, du preparedStatement et de la connexion
			try {if (rs != null)rs.close();} catch (Exception t) {}
			try {if (ps != null)ps.close();} catch (Exception t) {}
			try {if (con != null)con.close();} catch (Exception t) {}
		}
		return retour;

	}
	/*public static void main(String[] args)  throws SQLException {

	 PatientDAO DAO=new PatientDAO();

	Patient a = new Patient("me","me",22,1,"etudiant","someone");
	int retour=DAO.ajouter(a);

	System.out.println(retour+ " lignes ajoutées");

	Patient a2 = DAO.getPatient(1);
	System.out.println(a2.toString());
	a2.setPrenom("me2");
	DAO.modifier(a2);
	Patient a3 = DAO.getPatient(1);
	System.out.println(a3.toString());
	
	
	List<Patient> liste=DAO.getListePatients();
	//System.out.println(liste);
	for(Patient p : liste)
	{
		System.out.println(p.toString());
	}
	
	DAO.supprimer(a3);

}*/

}
