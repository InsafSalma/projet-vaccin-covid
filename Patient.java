package modules;

public class Patient {

	private int id;
	private String nom;
	private String prenom;
	private int age;
	private int phase;
	private String profession;
	private String nomMedecinTraitant;
	public Patient(String nom, String prenom, int age, int phase, String profession, String nomMedecinTraitant) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.phase = phase;
		this.profession = profession;
		this.nomMedecinTraitant = nomMedecinTraitant;
	}
	
	public Patient(int id, String nom, String prenom, int age, int phase, String profession,String nomMedecinTraitant) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.phase = phase;
		this.profession = profession;
		this.nomMedecinTraitant = nomMedecinTraitant;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getPhase() {
		return phase;
	}
	public void setPhase(int phase) {
		this.phase = phase;
	}
	public String getProfession() {
		return profession;
	}
	public void setProfession(String profession) {
		this.profession = profession;
	}
	public String getNomMedecinTraitant() {
		return nomMedecinTraitant;
	}
	public void setNomMedecinTraitant(String nomMedecinTraitant) {
		this.nomMedecinTraitant = nomMedecinTraitant;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", age=" + age + ", phase=" + phase
				+ ", profession=" + profession + ", nomMedecinTraitant=" + nomMedecinTraitant + "]";
	}
	
	
}
